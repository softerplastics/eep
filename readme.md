# eep

eep: An IP address display daemon


### DISCLAIMER
This software and its documentation are amateur-hour stuff and, as such, are provided without warranties of absolutely any kind.

### THE PROBLEM
I have a Raspberry Pi B+ that I run via an SSH connection.
Unfortunately, my building frequently suffers power interruptions, leading to the Pi's IP address being reassigned. I need to know the new address in order to SSH in. 

### MY SOLUTION
`eep.py` runs as a systemd service to drive 7-segment LEDs, displaying the IP address of the Pi an octet at a time. While an SSH session is active, the display stops.

### REQUIREMENTS AND DEPENDENCIES
+ Python 2.x
+ netifaces, RPi.GPIO modules for Python
+ 3x 7-segment numeric led displays
+ Raspberry Pi running Linux
+ resistors, leads etc suitable for connecting your model of display to the Pi's GPIO

### INSTALLATION
0. Connect three 7-segment LEDs to your RPi GPIO pins using leads and resistors. If your displays have a decimal point pin, this can be left unconnected. Make a note of which pin connects to each segment of a display.
1. Install the netifaces and RPi.GPIO modules (if you have pip, try `pip install <module>`).
2. Make a directory under your `/home/your_username/` directory and `cd` to it. Clone the eep repo to this location using `git clone git@bitbucket.org:softerplastics/eep.git` and then move (`mv source_file destination`) each file to an appropriate location. Make a note of where each file goes because you'll need it later.
    + `eep.py` should go to `/bin` or wherever your system binaries go. Let's call this Location A. `cd` to this location then `sudo chmod u+x eep.py` to set permissions.
    + `sevenseg.py` can go wherever you keep python modules you've written, or a directory under home. This is Location B. Make a new blank file in this location called `__init__.py` if one doesn't already exist there.
    + `eep.service` should go where systemd's `.service` files are kept, which is usually the `/etc/systemd/system` directory. This is Location C.
    + Find the path to your python binary (typically `/bin/python`).  This is Location D. You don't need to move any files here.
3. In a text editor, go through `eep.py` and make sure the paths mentioned at lines 10 (or nearby) match the path to your Location B directory. Look for the comments. Make sure that the path mentioned at line 1 points to Location D and has `#!` in front of it. Go through line 42 (which begins `led_list = `)and make sure that the pin numbers correspond to the pins you are using for eep's LEDs. The displays are listed as if being read left-to-right. Note that by default the `BOARD` numbering scheme (not the `BCM` numbering scheme) is used. See the next section for more detail.
4. In a text editor, go through `eep.service` and find line 11 (or nearby) which begins `ExecStart=`. Make sure the rest of the line matches the path to your Location A directory.
5. In a text editor, open `~/.bashrc` and add the contents of put_in_bash.txt to the end of the file. You may need to use `sudo` when editing .bashrc.
6. You should now be able to enable eep to start on boot: `sudo systemctl enable eep`

### CONNECTING 7-SEGMENT DISPLAYS
7-segment displays come in a variety of configurations, so it is recommended that you consult a pinout or datasheet for the display you are using. The information below is for the display used in my setup.

Pin positions (viewing front face of display):
```
0 1 a 2 3

 
4 5 b 6 c
```
These pins control the LEDs in the following positions:
```
  222
 1   3
 1   3
 1   3
  000
 4   6
 4   6
 4   6
  555  c
```
(pins a and b are grounds, pin c is not used in this project)

Once you've connected the LED pins to the GPIO pins, edit eep.py to ensure that the correct pins are being turned on and off. Line 42 of eep.py should look something like this like this:
`led_list = [sevenseg.SevDisplay(35, 36, 33, 31, 32, 29, 26), sevenseg.SevDisplay(23, 24, 21, 22, 19, 15, 18), sevenseg.SevDisplay(13, 16, 11, 12, 10, 7, 8)]`
That looks intimidating, doesn't it? Let's break it down:
```
                                                      pin6
                                                  pin5   |
                                              pin4   |   |
                                          pin3   |   |   |
                                      pin2   |   |   |   |
                                  pin1   |   |   |   |   |
                              pin0   |   |   |   |   |   |
                                 |   |   |   |   |   |   |
                                 v   v   v   v   v   v   v
led_list = [ sevenseg.SevDisplay(35, 36, 33, 31, 32, 29, 26), sevenseg.SevDisplay(23, 24, 21, 22, 19, 15, 18), sevenseg.SevDisplay(13, 16, 11, 12, 10, 7, 8) ]
             |______________________________________________| |______________________________________________| |____________________________________________|
                                       |                                       |                                       |
                                       |                                       |                                       |
                                       |______________________________        _|       ________________________________|
                                                                      |       |       |
                                                                      V       V       V
                                                                     ____    ____    ____ 
                                                                    |   |   |   |   |   |
                                                                    | 8 |   | 8 |   | 8 |
                                                                    |___|   |___|   |___|
                                                                      ^       ^       ^
                                                                      |       |       |
                                                     		     100s Digit   |       |
                                                                           10s Digit  | 
                                                                                     1s Digit
```
So, GPIO pin 35 on your Pi would correspond to the middle horizontal segment on the display showing the 100s digit.

### CONTACT
softerplastics@mail.com

### THANKS TO
+ Contributors to the modules on which eep relies
+ Community members on the Raspberry Pie discord server
+ JIMBLOM and MTAYLOR @ sparkfun for this tutorial https://learn.sparkfun.com/tutorials/raspberry-gpio/introduction
+ Ben Morel for this tutorial https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
+ Approximate Engineering for this tutorial https://approximateengineering.org/2017/04/running-python-as-a-linux-service/
+ StackOverflow users 

### GR33TZ
`W0RM5C#47`