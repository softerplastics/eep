#!/bin/python2.7


from shutil import copyfile

paths = {}
print('Attempting to copy:')
with open('/home/pi/eep_repo/eep_files.txt', 'r') as f:
    for line in f:
        key, value = line.split("=")
        paths[key.strip()] = value.strip()
        print(key + " to " + paths[key])

for key in paths:
    try:
        copyfile(key, paths[key])
    except:
        print("Hit a snag getting " + key + ". Aborting.")
        print("(maybe check that " + key + " exists)")
        break
