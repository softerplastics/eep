# Pin positions (from front face of display)
# 0 1 a 2 3
#
# 4 5 b 6 c
#
# LED positions
#
#  222
# 1   3
# 1   3
# 1   3
#  000
# 4   6
# 4   6
# 4   6
#  555  c
#
# (pins a and b are grounds)

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
class SevDisplay(object):
    def __init__ (self, pin0, pin1, pin2, pin3, pin4, pin5, pin6):
        # make a list of pins indexed by pin position
        self.segment = [pin0, pin1, pin2, pin3, pin4, pin5, pin6]
        for i, pin in enumerate(self.segment):
	    # print(pin)
            GPIO.setup(pin, GPIO.OUT)

    def clear(self):
        for i, pin in enumerate(self.segment):
            GPIO.output(pin, GPIO.LOW)

    def seg_print(self, digit):
        if digit < 10 and digit > -1:
            for i, pin in enumerate(self.segment):
                state = seg_led.number[digit]
                if state[i] == "h":
                    GPIO.output(pin, GPIO.HIGH)
        else:
            # non-numerical argument. panic
            pass

    def seg_test(self, led):
        light = self.segment[led]
        GPIO.output(light, GPIO.HIGH)

class SevShapes(object):
    def __init__(self):
        self.number = ["lhhhhhh", "lllhllh", "hlhhhhl", "hlhhlhh", "hhlhllh", "hhhllhh", "hhhlhhh", "llhhllh", "hhhhhhh", "hhhhlhh"]
        global seg_led
        seg_led = self
