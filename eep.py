#!/bin/python2.7

import signal
from time import sleep
import sys
import os
sys.path.append('/home/pi/.local/lib/python2.7/site-packages')
import netifaces as ni
sys.path.append('/home/pi')
from python_files import sevenseg

# this from https://stackoverflow.com/a/24196955
ni.ifaddresses('eth0')
address = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
# print address # end stackoverflow

# this from https://stackoverflow.com/a/31464349
class GracefulKiller(object):
    bashing = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.bashing = True
# end stackoverflow... more follows below

# split the address and insert a blank octet

first, second, third, fourth = address.split(".")

fifth = "   "

octets = [first, second, third, fourth, fifth]

for i, oct in enumerate(octets):
    while len(octets[i]) < 3:
        octets[i] = " " + octets[i]
    # print octets[i]
# initialise sevenseg
sevenseg.SevShapes()
led_list = [sevenseg.SevDisplay(35, 36, 33, 31, 32, 29, 26), sevenseg.SevDisplay(23, 24, 21, 22, 19, 15, 18), sevenseg.SevDisplay(13, 16, 11, 12, 10, 7, 8)]

# loop thru octets, print them

j = 0
killer = GracefulKiller()

while True:
    if j == len(octets):
        # reset to start of octets
        j = 0
    digits = octets[j]

    for i, led in enumerate(led_list):
        shape = digits[i]
        led.clear()
        if shape != " ":
            shape = int(shape)
            led.seg_print(shape)
            # print shape
    j += 1
    sleep(1.5)
    if killer.bashing:
        os.system('echo eep was stopped gracefully | wall')
        break

for led in led_list:
    led.clear()
sevenseg.GPIO.cleanup()
