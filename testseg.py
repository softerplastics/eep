import sevenseg
from time import sleep

sevenseg.SevenSegShape()
led_list = [sevenseg.SevenSeg(35, 36, 33, 31, 32, 29, 26), sevenseg.SevenSeg(23, 24, 21, 22, 19, 15, 18), sevenseg.SevenSeg(13, 16, 11, 12, 10, 7, 8)]
while 1:
    try:
        for no in range(0,10):
            for led in led_list:
                led.clear()
                led.seg_print(no)
		print no
            sleep(1)

    except KeyboardInterrupt:
        break

for led in led_list:
    led.clear()
sevenseg.GPIO.cleanup()
