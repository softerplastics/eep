#!/bin/python2.7


from shutil import copyfile

paths = {}
print('Attempting to copy:')
with open('/home/pi/eep_repo/eep_files.txt', 'r') as f:
    for line in f:
        value, key = line.split("=")
        paths[key.strip()] = value.strip()

for key in paths:
    try:
        print(key + " to " + paths[key])
        copyfile(key, paths[key])
        #print("   SUCCESS")
    except:
        print("Hit a snag putting " + key + ". Aborting.")
        print("(maybe check that " + key + " exists)")
        break
